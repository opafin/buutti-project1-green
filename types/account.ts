export interface Account {
  account_id: number
  account_user_id: number
  account_balance: number
}
