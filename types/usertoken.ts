import { JwtPayload } from 'jsonwebtoken'

export interface Usertoken extends JwtPayload {
  user_id: number
}
