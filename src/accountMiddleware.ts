import { Request, Response, NextFunction } from 'express'
import { checkUsersAccounts } from './accountDao'
import { Account } from '../types/account'

export const validateNewAccount = async (req: Request, res: Response, next: NextFunction) => {
  if (req.body === undefined)
    return res.status(404).send({ error: 'req.body is undefined, at least an account_user_id is required' })

  if (req.body.account_user_id === undefined)
    return res.status(404).send({ error: 'req.body.account_user_id is undefined, define a user_id' })

  res.locals.user_id
  if (req.body.account_user_id !== res.locals.user_id)
    return res.status(404).send({ error: 'can only create new accounts for yourself' })

  req.body.account_balance = Number(req.body.account_balance)
  if (typeof req.body.account_balance !== 'number')
    res.status(404).send({ error: 'account balance needs to be a number' })

  const over3Accounts = (await checkUsersAccounts(req.body.account_user_id)).length >= 3
  if (over3Accounts) return res.status(404).send({ error: 'you already have 3 accounts' })

  next()
}
export const getAuthedUsersAccounts = async (req: Request, res: Response, next: NextFunction) => {
  const account_id = Number(req.params.id)
  const user_id = res.locals.user_id
  const usersAccounts = await checkUsersAccounts(Number(user_id))
  console.log(usersAccounts, 'usersAccounts')
  const match = usersAccounts.find((account: Account) => {
    if (account.account_user_id === account_id) return account
  })
  console.log(match, 'match')
  if (match === undefined) return res.status(401).send('You are not authorized to access this account')
  if (match.account_user_id !== user_id) return res.status(401).send('You are not authorized to access this account')

  next()
}
