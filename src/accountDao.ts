import { executeQuery, pool } from './db'

export const transferMoney = async (transfer_sum: number, account_id: number, target_account_id: number) => {
  const client = await pool.connect()
  try {
    await client.query('BEGIN')
    await client.query('UPDATE accounts SET account_balance = account_balance - $1 WHERE account_id = $2', [ transfer_sum, account_id ])
    await client.query('UPDATE accounts SET account_balance = account_balance + $1 WHERE account_id = $2', [ transfer_sum, target_account_id ])
    await client.query('COMMIT')
    const source_account_balance = await client.query('SELECT account_balance FROM accounts WHERE account_id = $1', [ account_id ])
    const target_account_balance = await client.query('SELECT account_balance FROM accounts WHERE account_id = $1', [ target_account_id ])
    return `Transaction succesful. Transfered ${transfer_sum}€ from source account ${account_id} to target account ${target_account_id}. 
            Target account ${target_account_id} balance is now ${target_account_balance.rows[0].account_balance}€. 
            Source account ${account_id} balance is now ${source_account_balance.rows[0].account_balance}€.`
  } catch (error) {
    await client.query('ROLLBACK')
    return `transaction unsuccesful, transaction cancelled`
  } finally {
    client.release()
  }
}

export const deposit = async (deposit_sum: number, account_id: number) => {
    const query = `
    UPDATE accounts
    SET account_balance = account_balance + $1
    WHERE account_id = $2
    RETURNING *
    `
    const params = [deposit_sum, account_id]
    const result = await executeQuery(query, params)
    return result.rows[0]
}

export const withdraw = async (withdraw_sum: number, account_id: number) => {
    const query = `
    UPDATE accounts
    SET account_balance = account_balance - $1
    WHERE account_id = $2
    RETURNING *
    `
    const params = [withdraw_sum, account_id]
    const result = await executeQuery(query, params)
    return result.rows[0]
}

export const closeAccount = async (account_id:number) => {
    const query = `
    DELETE FROM accounts 
    WHERE account_id = $1
    `
    const params =[account_id]
    const result = await executeQuery(query, params)
    return result
}

export const createNewAccount = async (account_user_id: number, account_balance: number) => {
    const query = `
    INSERT INTO accounts (account_user_id, account_balance)
    VALUES ($1, $2)
    RETURNING *
    `
    const params = [account_user_id, account_balance]
    const result = await executeQuery(query, params)
    return result
    }

export const checkAllAccounts = async () => { 
  const query = `
  SELECT * FROM accounts
  `
  const result = await executeQuery(query)
  return result.rows
}

export const checkUsersAccounts = async (account_user_id: number) => {
    const query = `
    SELECT * FROM accounts
    WHERE account_user_id = $1
    `
    const params = [account_user_id]
    const result = await executeQuery(query, params)
    console.log(result.rows);
    return result.rows
}

export const checkBalance = async (account_id: number) => {
    const query = `
    SELECT account_balance FROM accounts
    WHERE account_id = $1
    `
    const params = [account_id]
    const result = await executeQuery(query, params)
    return result.rows[0]
    }
