import { Request, Response, NextFunction } from 'express'
import jwt from 'jsonwebtoken'
import { executeQuery } from './db'
import { Usertoken } from '../types/usertoken'

export const checkReqBodyValues = (req: Request, res: Response, next: NextFunction) => {
  if (req.body === undefined) return res.status(400).send({ error: 'req.body is undefined' })
  const { username, password } = req.body
  if ((username === undefined || username === '') && (password === undefined || password === undefined))
    return res.status(400).send({ error: 'Missing username or password.' })
  next()
}

export const checkIsUserExist = async (username: string) => {
  const query = 'SELECT * FROM "users"'
  const users = await executeQuery(query)
  const findUser = users.rows.find((user) => user.user_username === username)

  if (findUser !== undefined) return true

  return false
}

export const checkIsPasswordCorrect = async (username: string, password: string) => {
  const query = 'SELECT * FROM "users" WHERE user_username = $1'
  const params = [username]
  const users = await executeQuery(query, params)
  const findPassword = users.rows.find((user) => user.user_password === password)
  if (findPassword === undefined) return false

  return users.rows[0].user_id as number
}

export const returnJWT = (user_id: number) => {
  const payload = { user_id: user_id }
  if (!process.env.SECRET) {
    throw new Error('SECRET is not defined')
  }
  const secret = process.env.SECRET
  const token = jwt.sign(payload, secret)

  console.log(token)
  return token
}

export const verifyUserToken = async (req: Request, res: Response, next: NextFunction) => {
  const auth = req.get('Authorization')
  if (!auth?.startsWith('Bearer')) {
    return res.status(403).send('Not logged in')
  }
  const token = auth.substring(7)
  try {
    const decodedToken = jwt.verify(token, 'i think gollum graphics are great')
    const user: Usertoken = decodedToken as Usertoken
    res.locals.user_id = user.user_id
    next()
  } catch (error) {
    return res.status(401).send('Invalid token')
  }
}
