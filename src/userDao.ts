import { executeQuery } from './db'

export const deleteUser = async (id: number) => {
    const query = 'DELETE FROM "accounts" WHERE account_user_id=$1'
    const params = [id]
    await executeQuery(query, params)

    const queryTwo = 'DELETE FROM users WHERE user_id=$1'
    const paramsTwo = [id] 
    await executeQuery(queryTwo, paramsTwo)
    return
}

export const createNewUser = async (username: string, password: string) => {
  const query = 'INSERT INTO "users" (user_username, user_password) VALUES($1, $2) RETURNING *'
  const params = [username, password]
  const result = await executeQuery(query, params)
  const userid = result.rows[0].user_id

  const query2 = 'INSERT INTO accounts (account_user_id, account_balance) VALUES($1, $2)'
  const params2 = [userid, 0]
  await executeQuery(query2, params2)

  return userid
}
