import express, { Request, Response } from 'express'
import { accounts } from './accountRouter'
import userRouter from './userRouter'

export const createServer = () => {
  const server = express()
  server.use(express.json())
  server.use('/accounts', accounts)
  server.use('/users', userRouter)
  server.get('/', (req: Request, res: Response) => {
    res.send('Hello Green!')
  })
  return server
}
