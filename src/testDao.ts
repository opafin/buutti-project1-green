import { executeQuery } from './db'

export const createJuhannus = async () => {
  const query = `
          CREATE table Juhannustable (
              juhannusruoka varchar(255),
              juhannusjuoma varchar(255)
              )`
  const result = await executeQuery(query)
  return result.rows[0]
}
