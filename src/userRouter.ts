import express, { Request, Response } from 'express'
import { createNewUser } from './userDao'
import { checkReqBodyValues, checkIsUserExist, checkIsPasswordCorrect, returnJWT } from './middleware'

const router = express.Router()

router.post('/register', checkReqBodyValues, async (req: Request, res: Response) => {
  const { username, password } = req.body
  const validusername = await checkIsUserExist(username)

  if (validusername) return res.status(400).send({ error: 'username is already taken.' })

  const id = await createNewUser(username, password)
  const token = returnJWT(id)

  res.status(200).send({ token })
})

router.post('/login', checkReqBodyValues, async (req: Request, res: Response) => {
  const { username, password } = req.body
  const validusername = await checkIsUserExist(username)
  if (!validusername) return res.status(400).send({ error: 'username is already taken.' })

  const getUserId = await checkIsPasswordCorrect(username, password)
  if (!getUserId) return res.status(400).send({ error: 'password is incorrect' })

  const token = returnJWT(getUserId)

  res.status(200).send(token)
})

export default router
