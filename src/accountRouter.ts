import express, { Request, Response } from 'express'
import { closeAccount, transferMoney, createNewAccount, checkBalance } from './accountDao'
import { checkAllAccounts, checkUsersAccounts, withdraw } from './accountDao'
import { verifyUserToken } from './middleware'
import { deposit } from './accountDao'
import { getAuthedUsersAccounts, validateNewAccount } from './accountMiddleware'

export const accounts = express.Router()

accounts.patch('/:id/transfer', verifyUserToken, getAuthedUsersAccounts, async (req: Request, res: Response) => {
  const account_id = req.params.id
  const { transfer_sum, target_account_id } = req.body
  const result = await transferMoney(Number(transfer_sum), Number(account_id), Number(target_account_id))
  res.status(200).send(result)
})

// saa laittaa massia tilille ilman authia! tietysti tälläkin voisi kalastella mitkä
// tilinrot on valideja, mutta tulkoon massia!
accounts.patch('/:id/deposit', async (req: Request, res: Response) => {
  const account_id = req.params.id
  const deposit_sum = req.body.deposit_sum
  const result = await deposit(Number(deposit_sum), Number(account_id))
  res.status(200).send(result)
})

accounts.patch('/:id/withdraw', verifyUserToken, getAuthedUsersAccounts, async (req: Request, res: Response) => {
  const account_id = req.params.id
  const withdraw_sum = req.body.withdraw_sum
  const result = await withdraw(Number(withdraw_sum), Number(account_id))
  res.status(200).send(result)
})

accounts.delete('/:id', verifyUserToken, getAuthedUsersAccounts, async (req: Request, res: Response) => {
  const id = Number(req.params.id)
  const result = await closeAccount(id)
  res.status(200).send(result)
})

accounts.post('/', verifyUserToken, validateNewAccount, async (req: Request, res: Response) => {
  const account_user_id = req.body.account_user_id
  const account_balance = req.body.account_balance ?? 0
  const result = await createNewAccount(Number(account_user_id), Number(account_balance))
  res.status(200).send(result)
})
//prettier-ignore
accounts.get('/', verifyUserToken, /* verifadmin */ async (req: Request, res: Response) => {
  const result = await checkAllAccounts()
  res.status(200).send(result)
})

accounts.get('/:account_id', verifyUserToken, async (req: Request, res: Response) => {
  const account_id = Number(req.params.account_id)
  const result = await checkBalance(account_id)
  res.status(200).send(result)
})

accounts.get('/user/:user_id', verifyUserToken, async (req: Request, res: Response) => {
  const user_id = Number(req.params.user_id)
  const result = await checkUsersAccounts(user_id)
  res.status(200).send(result)
})
